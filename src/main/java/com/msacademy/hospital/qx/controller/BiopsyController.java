package com.msacademy.hospital.qx.controller;

import com.msacademy.hospital.qx.model.entity.Biopsy;
import com.msacademy.hospital.qx.model.repository.BiopsyRepository;
import com.msacademy.hospital.qx.response.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class BiopsyController {

    @Autowired
    private BiopsyRepository biopsyRepository;

    @GetMapping(value = "biopsy/{idBiopsy}")
    public ResponseEntity getBiopsy(@PathVariable(value = "idBiopsy") int idBiopsy){
        try{
            Biopsy biopsy = biopsyRepository.findById(idBiopsy);
            if(biopsy == null){
                return new ResponseEntity<>(
                        new Response(
                                HttpStatus.NOT_FOUND,"Cannot find Biopsy with id: " + idBiopsy), HttpStatus.NOT_FOUND);
            }
            else
                return new ResponseEntity(biopsy, HttpStatus.OK);
        }
        catch(Exception e){
            return new ResponseEntity<>(
                    new Response(
                            HttpStatus.BAD_REQUEST ,"An error has been occurred."), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = "biopsies")
    public ResponseEntity getBiopsies(){
        try{
            List<Biopsy> biopsies = biopsyRepository.findAll();
            if(biopsies.isEmpty()){
                return new ResponseEntity<>(
                        new Response(
                                HttpStatus.NOT_FOUND,"No Biopsies can be found"), HttpStatus.NOT_FOUND);
            }
            else
                return new ResponseEntity(biopsies, HttpStatus.OK);
        }
        catch(Exception e){
            return new ResponseEntity<>(
                    new Response(
                            HttpStatus.BAD_REQUEST ,"An error has been occurred."), HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping(value = "biopsyp/{idBiopsy}")
    public ResponseEntity updateBiopsy(@PathVariable int idBiopsy, @Valid @RequestBody Biopsy newBiopsy){
        Biopsy oldBiopsy = biopsyRepository.findById(idBiopsy);
        if(oldBiopsy == null){
            return new ResponseEntity<>(
                    new Response(
                            HttpStatus.NOT_FOUND,"Cannot find Biopsy with id: " + idBiopsy), HttpStatus.NOT_FOUND);
        }
        try{
            biopsyRepository.save(updateBiopsy(oldBiopsy, newBiopsy));
        }catch(Exception e){
            return new ResponseEntity<>(
                    new Response(
                            HttpStatus.BAD_REQUEST,"Cannot upgrade Biopsy with id: " + idBiopsy), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(
                new Response(
                        HttpStatus.ACCEPTED ,"Biopsy updated successfully"), HttpStatus.ACCEPTED);
    }

    private Biopsy updateBiopsy(Biopsy oldBiopsy, Biopsy newBiopsy){

        if(newBiopsy.getType() != null)
            oldBiopsy.setType(newBiopsy.getType());
        if(newBiopsy.getDescription() != null)
            oldBiopsy.setDescription(newBiopsy.getDescription());

        return oldBiopsy;
    }

}