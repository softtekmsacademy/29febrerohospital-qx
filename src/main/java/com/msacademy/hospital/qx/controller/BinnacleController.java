package com.msacademy.hospital.qx.controller;

import com.msacademy.hospital.qx.model.entity.Binnacle;
import com.msacademy.hospital.qx.model.repository.BinnacleRepository;
import com.msacademy.hospital.qx.response.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class BinnacleController {

    @Autowired
    private BinnacleRepository binnacleRepository;

    @GetMapping(value = "binnacle/{idBinnacle}")
    public ResponseEntity getBinnacle(@PathVariable(value = "idBinnacle") int idBinnacle){
        try{
            Binnacle binnacle = binnacleRepository.findById(idBinnacle);
            if(binnacle == null){
                return new ResponseEntity<>(
                        new Response(
                                HttpStatus.NOT_FOUND,"Cannot find Binnacle with id: " + idBinnacle), HttpStatus.NOT_FOUND);
            }else
                return new ResponseEntity(binnacle, HttpStatus.OK);

        }
        catch(Exception e){
            return new ResponseEntity<>(
                    new Response(
                            HttpStatus.BAD_REQUEST ,"An error has been occurred."), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = "binnacles")
    public ResponseEntity getBinnacles(){
        try{
            List<Binnacle> binnacles = binnacleRepository.findAll();
            if(binnacles.isEmpty()){
                return new ResponseEntity<>(
                        new Response(
                                HttpStatus.NOT_FOUND,"No Binnacles can be found"), HttpStatus.NOT_FOUND);
            }else
                return new ResponseEntity(binnacles, HttpStatus.OK);

        }
        catch(Exception e){
            return new ResponseEntity<>(
                    new Response(
                            HttpStatus.BAD_REQUEST ,"An error has been occurred."), HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping(value = "binnaclep/{idBinnacle}")
    public ResponseEntity updateBinnacle(@PathVariable int idBinnacle, @Valid @RequestBody Binnacle newBinnacle){
        Binnacle oldBinnacle = binnacleRepository.findById(idBinnacle);
        if(oldBinnacle == null){
            return new ResponseEntity<>(
                    new Response(
                            HttpStatus.NOT_FOUND,"Cannot find Binnacle with id: " + idBinnacle), HttpStatus.NOT_FOUND);
        }
        try{
            binnacleRepository.save(updateBinnacle(oldBinnacle, newBinnacle));
        }catch(Exception e){
            return new ResponseEntity<>(
                    new Response(
                            HttpStatus.BAD_REQUEST,"Cannot upgrade Binnacle with id: " + idBinnacle), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(
                new Response(
                        HttpStatus.ACCEPTED ,"Binnacle updated successfully"), HttpStatus.ACCEPTED);
    }

    private Binnacle updateBinnacle(Binnacle oldBinnacle, Binnacle newBinnacle){
        if(newBinnacle.getEntryTimeMedicalStaff() != null)
            oldBinnacle.setEntryTimeMedicalStaff(newBinnacle.getEntryTimeMedicalStaff());
        if(newBinnacle.getEntryTimePatient() != null)
            oldBinnacle.setEntryTimePatient(newBinnacle.getEntryTimePatient());
        if(newBinnacle.getDepartureTimeMedicalStaff() != null)
            oldBinnacle.setDepartureTimeMedicalStaff(newBinnacle.getDepartureTimeMedicalStaff());
        if(newBinnacle.getDepartureTimePatient() != null)
            oldBinnacle.setDepartureTimePatient(newBinnacle.getDepartureTimePatient());
        if(newBinnacle.getStartTimeSurgery() != null)
            oldBinnacle.setStartTimeSurgery(newBinnacle.getStartTimeSurgery());
        if(newBinnacle.getEndTimeSurgery() != null)
            oldBinnacle.setEndTimeSurgery(newBinnacle.getEndTimeSurgery());

        return oldBinnacle;
    }

}