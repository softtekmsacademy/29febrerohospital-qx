package com.msacademy.hospital.qx.controller;

import com.msacademy.hospital.qx.model.entity.SurgicalIntervention;
import com.msacademy.hospital.qx.model.repository.BinnacleRepository;
import com.msacademy.hospital.qx.model.repository.BiopsyRepository;
import com.msacademy.hospital.qx.model.repository.SurgicalInterventionRepository;
import com.msacademy.hospital.qx.response.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class SurgicalInterventionController {

    @Autowired
    private SurgicalInterventionRepository surgicalInterventionRepository;

    @Autowired
    private BiopsyRepository biopsyRepository;

    @Autowired
    private BinnacleRepository binnacleRepository;

    @GetMapping(value = "surgical-intervention/{idSurgicalIntervention}")
    public ResponseEntity getSurgicalIntervention(@PathVariable(value = "idSurgicalIntervention") int idSurgicalIntervention){
        try{
            SurgicalIntervention surgicalIntervention = surgicalInterventionRepository.findById(idSurgicalIntervention);
            if(surgicalIntervention == null){
                return new ResponseEntity<>(
                        new Response(
                                HttpStatus.NOT_FOUND,"Cannot find Surgical Intervention with id: " + idSurgicalIntervention), HttpStatus.NOT_FOUND);
            }
            else
                return new ResponseEntity(surgicalIntervention, HttpStatus.OK);
        }
        catch(Exception e){
            return new ResponseEntity<>(
                    new Response(
                            HttpStatus.BAD_REQUEST ,"An error has been occurred."), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(value = "surgical-interventions")
    public ResponseEntity getSurgicalInterventions(){
        try{
            List<SurgicalIntervention> surgicalInterventions = surgicalInterventionRepository.findAll();
            if(surgicalInterventions.isEmpty()){
                return new ResponseEntity<>(
                        new Response(
                                HttpStatus.NOT_FOUND,"No Surgical Interventions can be found"), HttpStatus.NOT_FOUND);
            }
            else
                return new ResponseEntity(surgicalInterventions, HttpStatus.OK);
        }
        catch(Exception e){
            return new ResponseEntity<>(
                    new Response(
                            HttpStatus.BAD_REQUEST ,"An error has been occurred."), HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(value = "/surgical-intervention",headers = "Accept=application/json")
    public ResponseEntity<?> setSurgicalIntervention(@Valid @RequestBody SurgicalIntervention surgicalIntervention){
        try{
            surgicalInterventionRepository.save(surgicalIntervention);
        }catch (Exception e){
            return new ResponseEntity<>(
                    new Response(
                            HttpStatus.BAD_REQUEST ,"An error has been occurred."), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(
                new Response(
                        HttpStatus.CREATED ,"Surgical Intervention added successfully."), HttpStatus.CREATED);
    }

    @PutMapping(value = "surgicalp/{idSurgicalIntervention}")
    public ResponseEntity updateSurgicalIntervention(@PathVariable int idSurgicalIntervention, @Valid @RequestBody SurgicalIntervention newSurgicalIntervention){
        SurgicalIntervention oldSurgicalIntervention = surgicalInterventionRepository.findById(idSurgicalIntervention);
        if(oldSurgicalIntervention == null){
            return new ResponseEntity<>(
                    new Response(
                            HttpStatus.NOT_FOUND,"Cannot find Surgical Intervention with id: " + idSurgicalIntervention), HttpStatus.NOT_FOUND);
        }
        try{
            surgicalInterventionRepository.save(updateSurgicalIntervention(oldSurgicalIntervention, newSurgicalIntervention));
        }catch(Exception e){
            return new ResponseEntity<>(
                    new Response(
                            HttpStatus.BAD_REQUEST,"Cannot upgrade Surgical Intervention with id: " + idSurgicalIntervention), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(
                new Response(
                        HttpStatus.ACCEPTED ,"Surgical Intervention updated successfully"), HttpStatus.ACCEPTED);
    }

    @DeleteMapping(value = "surgicald/{idSurgicalIntervention}")
    public ResponseEntity<?> deleteSurgicalIntervention(@PathVariable int idSurgicalIntervention){
        SurgicalIntervention surgicalIntervention = surgicalInterventionRepository.findById(idSurgicalIntervention);
        if(surgicalIntervention == null){
            return new ResponseEntity<>(
                    new Response(
                            HttpStatus.NOT_FOUND,"Cannot find Surgical Intervention with id: " + idSurgicalIntervention), HttpStatus.NOT_FOUND);
        }
        try{
            surgicalInterventionRepository.delete(surgicalIntervention);
            biopsyRepository.delete(surgicalIntervention.getBiopsy());
            binnacleRepository.delete(surgicalIntervention.getBinnacle());
        }catch(Exception e){
            return new ResponseEntity<>(
                    new Response(
                            HttpStatus.BAD_REQUEST,"Cannot delete Surgical Intervention with id: " + idSurgicalIntervention), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(
                new Response(
                        HttpStatus.ACCEPTED ,"Surgical Intervention deleted successfully"), HttpStatus.ACCEPTED);
    }

    private SurgicalIntervention updateSurgicalIntervention(SurgicalIntervention oldSurgicalIntervention, SurgicalIntervention newSurgicalIntervention){

        if(newSurgicalIntervention.getInterventionProcedure() != null)
            oldSurgicalIntervention.setInterventionProcedure(newSurgicalIntervention.getInterventionProcedure());
        if(newSurgicalIntervention.getDuration()>0)
            oldSurgicalIntervention.setDuration(newSurgicalIntervention.getDuration());
        if(newSurgicalIntervention.getIdSurgicalInterventionCat()>0)
            oldSurgicalIntervention.setIdSurgicalInterventionCat(newSurgicalIntervention.getIdSurgicalInterventionCat());
        if(newSurgicalIntervention.getIdImagingResult()>0)
            oldSurgicalIntervention.setIdImagingResult(newSurgicalIntervention.getIdImagingResult());

        return oldSurgicalIntervention;
    }

}