package com.msacademy.hospital.qx.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

@Entity
@Table(name = "surgical_intervention")
@JsonIgnoreProperties(value = {"createdAt", "updatedAt"}, allowGetters = true)
public class SurgicalIntervention {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idSurgicalIntervention;

    @Column(name = "intervention_procedure")
    private String interventionProcedure;

    private int duration;

    @OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name="id_binnacle")
    private Binnacle binnacle;

    @OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name="id_biopsy")
    private Biopsy biopsy;

    @JoinColumn(name="id_surgical_intervention_cat")
    private int idSurgicalInterventionCat;

    @JoinColumn(name="id_imaging_result")
    private int idImagingResult;

    public int getIdSurgicalIntervention() {
        return idSurgicalIntervention;
    }

    public void setIdSurgicalIntervention(int idSurgicalIntervention) {
        this.idSurgicalIntervention = idSurgicalIntervention;
    }

    public String getInterventionProcedure() {
        return interventionProcedure;
    }

    public void setInterventionProcedure(String interventionProcedure) {
        this.interventionProcedure = interventionProcedure;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public Binnacle getBinnacle() {
        return binnacle;
    }

    public void setBinnacle(Binnacle binnacle) {
        this.binnacle = binnacle;
    }

    public Biopsy getBiopsy() {
        return biopsy;
    }

    public void setBiopsy(Biopsy biopsy) {
        this.biopsy = biopsy;
    }

    public int getIdSurgicalInterventionCat() {
        return idSurgicalInterventionCat;
    }

    public void setIdSurgicalInterventionCat(int idSurgicalInterventionCat) {
        this.idSurgicalInterventionCat = idSurgicalInterventionCat;
    }

    public int getIdImagingResult() {
        return idImagingResult;
    }

    public void setIdImagingResult(int idImagingResult) {
        this.idImagingResult = idImagingResult;
    }
    
}