package com.msacademy.hospital.qx.model.repository;

import com.msacademy.hospital.qx.model.entity.SurgicalIntervention;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SurgicalInterventionRepository extends CrudRepository<SurgicalIntervention,Integer> {

    @Query("SELECT s FROM SurgicalIntervention s WHERE s.idSurgicalIntervention = ?1")
    SurgicalIntervention findById(@Param("idSurgicalIntervention") int idSurgicalIntervention);

    @Query("SELECT s FROM SurgicalIntervention s WHERE s.idSurgicalIntervention > 0")
    List<SurgicalIntervention> findAll();

}